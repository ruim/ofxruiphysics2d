# ofxRuiPhysics2D

A C++, 2D Verlet physics implementation for OpenFrameworks
It is based on the paper [Advanced Character Physics](https://www.cs.cmu.edu/afs/cs/academic/class/15462-s13/www/lec_slides/Jakobsen.pdf) by Thomas Jakobsen

## Features
    - A Particle class implementing the Verlet integrator with methods to apply forces, impules, define properties such as mass, drag, etc
    - A Spring class implementing a distance constraint between particles
    - A basic collision solver between particles

## Getting started

- Download OpenFrameworks from www.openframeworks.cc
- Create a new project
- Copy the src folder to the addons folder of your project and add it to the header search path of your project
- *OPTIONAL* Replace the files in your project with the files in the example folder if you want to explore at the provided example

## License
MIT license - https://opensource.org/licenses/MIT

## Roadmap
This library has not been updated in a while and will only be updated in case I require to use it on a new project (sorry..)

## Author
Rui Madeira